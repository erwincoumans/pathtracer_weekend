#ifndef CAMERA_H
#define CAMERA_H

class camera
{
    vec3 m_lower_left_corner;
    vec3 m_horizontal;
    vec3 m_vertical;
    vec3 m_origin;

  public:
    camera()
    {
      m_lower_left_corner = vec3(-2.0, -1.0, -1.0);
      m_horizontal = vec3(4.0,0.0,0.0);
      m_vertical = vec3(0.0,2.0,0.0);
      m_origin = vec3(0,0,0);
    }
    ray get_ray(float u, float v)
    {
    	return ray(m_origin, m_lower_left_corner + u*m_horizontal+v*m_vertical-m_origin);
    }
};

#endif

