#ifndef VEC3_H
#define VEC3_H

#include "btVector3.h"
typedef btVector3 vec3;
inline vec3 unit_vector(const vec3& v)
{
    return v.normalized();
}

inline float dot(const vec3& a, const vec3& b)
{
    return btDot(a,b);
}

inline vec3 random_in_unit_sphere()
{
    vec3 p;
    do
    {
        p=2.0*vec3(drand48(),drand48(), drand48()) - vec3(1,1,1);
    } while (p.length2() >= 1.0);
    return p;
}

#endif //VEC3_H

