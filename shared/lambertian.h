#ifndef LAMBERTIAN_H
#define LAMBERTIAN_H

#include "material.h"
#include "ray.h"

class lambertian : public material
{
    vec3 m_albedo;
  public:
    lambertian(const vec3& albedo) : m_albedo(albedo) {}
    virtual bool scatter(const ray& r_in, const hit_record& rec, vec3& attenuation, ray& scattered) const
    {
        vec3 target=rec.p+rec.normal+random_in_unit_sphere();
        scattered = ray(rec.p, target-rec.p);
        attenuation = m_albedo;
        return true;
    }
};
#endif

