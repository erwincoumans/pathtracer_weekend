#ifndef MATERIAL_H
#define MATERIAL_H

struct hit_record;
#include "ray.h"

class material
{
  public:
    virtual bool scatter(const ray& r_in, const hit_record& rec, vec3& attenuation, ray& scattered) const = 0;
};

vec3 reflect(const vec3& v, const vec3& n)
{
    return v - 2.*dot(v,n)*n;
}

class metal : public material {
    public:
        metal(const vec3& a, float f) : m_albedo(a) { if (f < 1) m_fuzz = f; else m_fuzz = 1; }
        virtual bool scatter(const ray& r_in, const hit_record& rec, vec3& attenuation, ray& scattered) const  {
            vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
            scattered = ray(rec.p, reflected + m_fuzz*random_in_unit_sphere());
            attenuation = m_albedo;
            return (dot(scattered.direction(), rec.normal) > 0);
        }
        vec3 m_albedo;
        float m_fuzz;
};

#endif

