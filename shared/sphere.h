#ifndef SPHERE_H
#define SPHERE_H

#include "hitable.h"

class sphere : public hitable
{
    vec3 m_center;
    float m_radius;
    material* m_material;

  public:
    sphere() {}
    sphere(const vec3& center, float r, material* mat) : m_center(center), m_radius(r), m_material(mat) {}
    virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;
};

bool sphere::hit(const ray& r, float t_min, float t_max, hit_record& rec) const
{
    vec3 oc = r.origin() - m_center;
    float a = dot(r.direction() , r.direction());
    float b = dot(oc, r.direction());
    float c = dot(oc,oc) - m_radius*m_radius;
    float temp = (-b - sqrt(b*b-a*c))/a;
    if (temp < t_max && temp > t_min)
    {
        rec.t = temp;
        rec.p = r.point_at_parameter(rec.t);
        rec.normal = (rec.p - m_center) / m_radius;
        rec.m_mat_ptr = m_material;
        return true;
    }

    temp = (-b + sqrt(b*b-a*c))/a;
    if (temp < t_max && temp > t_min) 
    {
        rec.t = temp;
        rec.p = r.point_at_parameter(rec.t);
        rec.normal = (rec.p - m_center) / m_radius;
        rec.m_mat_ptr = m_material;
        return true;
    }
    return false;
}
#endif
